<?php

/**
 * @file
 * Defines additional tokens for order email.
 * 
 * @author Md Shariful Islam.    <https://drupal.org/user/643640>
 */

/**
 * Implements hook_token_info_alter().
 */
function commerce_fedex_shipping_token_info_alter(&$data) {
  $data['tokens']['commerce-order']['commerce-email-fedex-tracking'] = array(
    'name' => t('Fedex Tracking'),
    'description' => t('Fedex Tracking Description.'),
  );
}

/**
 * Implements hook_tokens().
 */
function commerce_fedex_shipping_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $url_options = array('absolute' => TRUE);

  if (isset($options['language'])) {
    $url_options['language'] = $options['language'];
    $language_code = $options['language']->language;
  }
  else {
    $language_code = NULL;
  }

  $sanitize = !empty($options['sanitize']);

  $replacements = array();

  if ($type == 'commerce-order' && !empty($data['commerce-order'])) {
    $order = $data['commerce-order'];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'commerce-email-fedex-tracking':
            
            $order_items = '';
        //  $order_items = commerce_email_order_items($order);
          $replacements[$original] = $order_items;
          break;       
          
      }
      
      
    }
  }
  return $replacements;
}





