<?php

/**
 * @file
  * The core module file for FedEx Services.
 *
 * Shipping quote module that interfaces with the FedEx Web Services API
 * to get rates for small package shipments.  Implements a SOAP Web Service
 * client.
 *
 * @author Md Shariful Islam.    <https://drupal.org/user/643640>
 */


/** Maximum shipping weight for FedEx (non-Freight services) */
 define('PACKAGE_WEIGHT_LIMIT_LBS', 150.0);  // 150lbs

// Set to 0 to disable caching of SOAP WSDL when developing your WSDL
 ini_set("soap.wsdl_cache_enabled", "1");

/******************************************************************************
 * Commerce Hooks                                                               *
 ******************************************************************************/

/**
 * Implements hook_commerce_shipping_method_info().
 */
function commerce_fedex_shipping_commerce_shipping_method_info() {
  $shipping_methods = array();

  $shipping_methods['fedex'] = array(
    'title' => t('FedEx'),
    'description' => t('Quote rates from FedEx'),
  );
  
  

  return $shipping_methods;
}

/**
 * Implements hook_commerce_shipping_service_info().
 */
function commerce_fedex_shipping_commerce_shipping_service_info() {
  $shipping_services = array();
  $available_services = commerce_fedex_shipping_shipping_service_types();
  $selected_services = variable_get('commerce_fedex_shipping_services', array());
  foreach ($selected_services as $id => $val) {
    if (!empty($val)) {
      $service = $available_services[$id];
      $shipping_services[drupal_strtolower($id)] = array(
        'title' => $service,
        'description' => $service,
        'display_title' => $service,
        'shipping_method' => 'fedex',
        'price_component' => 'shipping',
        'callbacks' => array(
          'rate' => 'commerce_fedex_shipping_service_rate',
        ),
      );
    }
  }
  return $shipping_services;
}


/**
 * Shipping service callback: returns a base price array for a shipping service
 * calculated for the given order.
 */
function commerce_fedex_shipping_service_rate($shipping_service, $order) {
 module_load_include('inc', 'commerce_fedex_shipping', 'commerce_fedex_shipping_helper');   
 $items = _commerce_fedex_get_package_items($order);
 $destination =  commerce_customer_profile_load($order->commerce_customer_shipping['und'][0]['profile_id']);
 $response = commerce_fedex_shipping_rate_request($items, $destination);
 
  
 // $response =   commerce_fedex_shipping_shipment_request($items, strtoupper($shipping_service['name']), $destination);
 

 
 
 if (!isset($response->RateReplyDetails)) {
     
    // Memphis, we have a problem ...
    // Error returned from FedEx server - will print in $message box
    // Don't even try to extract a quote from the response, just return
    // empty quote array.
    return ;
  }
  
    

  // Test responses to see if we are interested in that service
  foreach ($response->RateReplyDetails as $options) {
    $service = $options->ServiceType;
  
    if ($service == strtoupper($shipping_service['name'])) {
        
      // Check to see if we're quoting ACCOUNT or LIST rates
      
        // LIST quotes return both ACCOUNT rates and LIST rates:
        // Order not guaranteed.
        //   RatedShipmentDetails[0] = PAYOR_ACCOUNT
        //   RatedShipmentDetails[1] = RATED_ACCOUNT
        //   RatedShipmentDetails[2] = PAYOR_LIST
        //   RatedShipmentDetails[3] = RATED_LIST
        foreach ($options->RatedShipmentDetails as $ratedetail) {
          if ($ratedetail->ShipmentRateDetail->RateType == 'PAYOR_LIST') {
            break;
          }
        }
      

      
    }
  }
      // need to handle dimensional rates, other modifiers.

      // Markup rate before customer sees it
     // $rate = commerce_fedex_shipping_rate_markup($ratedetail->ShipmentRateDetail->TotalNetCharge->Amount);
    
 if(!empty($ratedetail))  {  
  return array(
    'amount' => $ratedetail->ShipmentRateDetail->TotalNetCharge->Amount,
    'currency_code' => 'USD',
    'data' => array(),
  );
 } else {
     return;
 }
}

/**
 * Implements hook_commerce_shipping_service_rate_options_alter().
 */
function commerce_fedex_shipping_commerce_shipping_service_rate_options_alter(&$options, $order) {
  // If the display FedEx logo next to FedEx services is enabled in settings,
  // loop through the shipping options and add the FedEx logo to FedEx services.
 
    
    if (variable_get('commerce_fedex_shipping_show_logo', FALSE)) {
       
    $image = drupal_get_path('module', 'commerce_fedex_shipping') . '/fedex_logo.gif';
    if (file_exists($image)) {
         
      foreach ($options as $key => &$option) {
         
        if (in_array(drupal_strtoupper($key), array_keys(commerce_fedex_shipping_shipping_service_types()))) {
          $option = theme('image', array('path' => $image, 'width' => '32px')) . ' ' . $option;
        }
      }
    }
  }
}

 
 
 
 

/******************************************************************************
 * Drupal Hooks                                                               *
 ******************************************************************************/


/**
 * Implements hook_menu().
 */
function commerce_fedex_shipping_menu() {
  $items = array();
    
   $items['admin/commerce/config/shipping/methods/fedex/edit'] = array(
    'title' => 'Edit',
    'description' => 'Adjust FedEx shipping settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('commerce_fedex_shipping_settings_form'),
    'access arguments' => array('administer shipping'),
    'file' => 'commerce_fedex_shipping.admin.inc',
    'type' => MENU_LOCAL_TASK,
    'context' => MENU_CONTEXT_INLINE,
    'weight' => 0,
  );

  return $items;
}



/******************************************************************************
 * Module Functions                                                           *
 ******************************************************************************/




/**
 * Constructs and executes a SOAP RateAvailabilityService request.
 * Obtains Rate and Available Services information needed for
 * shipping quote.
 *
 * SOAP call parameters are set in the order they appear in the WSDL file
 * Associative array of DOM returned.
 *
 * @param $packages
 *   Array of packages received from the cart.
 * @param $origin
 *   Delivery origin address information.
 * @param $destination
 *   Delivery destination address information.
 *
 * @return
 *   Associative array mirroring contents of SOAP object returned from server.
 */
function commerce_fedex_shipping_rate_request($packages, $destination) {

  // Set up SOAP call.
  // Allow tracing so details of request can be retrieved for error logging
  $client = new SoapClient(drupal_get_path('module', 'commerce_fedex_shipping')
              . '/wsdl-' . variable_get('commerce_fedex_shipping_request_mode', 'testing')
              . '/RateService_v8.wsdl', array('trace' => 1));

  // FedEx user key and password filled in by user on admin form
  $request['WebAuthenticationDetail'] = array(
    'UserCredential' => array(
      'Key'      => variable_get('commerce_fedex_shipping_key', 0),
      'Password' => variable_get('commerce_fedex_shipping_password', 0),
    )
  );

  // FedEx account and meter number filled in by user on admin form
  $request['ClientDetail'] = array(
      'AccountNumber' => variable_get('commerce_fedex_shipping_account_number', 0),
      'MeterNumber'   => variable_get('commerce_fedex_shipping_meter_number', 5),
  );

  // Optional parameter, contains anything
  $request['TransactionDetail'] = array(
    'CustomerTransactionId' => '*** Rate/Available Services Request v8 from Ubercart ***'
  );

  // Rate Services Availability Request v8.0.0
  $request['Version'] = array(
    'ServiceId'    => 'crs',
    'Major'        => '8',
    'Intermediate' => '0',
    'Minor'        => '0',
  );

  // Grab details of sender origin - not necessarily package origin
  $request['RequestedShipment']['Shipper'] = array(
    'Address' => array(
      'PostalCode'  => variable_get('commerce_fedex_shipping_postal_code', 0),
      'CountryCode' =>  variable_get('commerce_fedex_shipping_country_code', 'US'),
    )
  );

  // Grab details of package destination
  
  // Grab details of package destination 
  $request['RequestedShipment']['Recipient'] = array(

    'Address' => array(      
      'PostalCode'          => $destination->commerce_customer_address['und'][0]['postal_code'],
      'CountryCode'         => $destination->commerce_customer_address['und'][0]['country'],
      'Residential'         => 1,
    )
  );


  // Currency for quote
  $request['RequestedShipment']['CurrencyType'] =  'USD';

  // Set Pickup/Dropoff type
  $request['RequestedShipment']['DropoffType'] = 'REGULAR_PICKUP';

  // Note that ACCOUNT rates *require* a valid account number
  // and return accurate answers on the production server
  $request['RequestedShipment']['RateRequestTypes'] = strtoupper(variable_get('commerce_fedex_shipping_rate_service_type', 'list'));

  // When the package is going to ship
// have to think about this -
// cutoff times, commits store owner to exact ship date, etc.
// Probably have to make an admin menu option with cutoff time, after
// which ShipDate becomes "tomorrow" unless of course tomorrow is a
// weekend when you're closed...  But this shouldn't affect the rate

  // Drupal 6.x version of format_date() doesn't support 'c', so until
  // then we use date() directly.  date() doesn't take care of site
  // timezone, though.
  //$request['RequestedShipment']['ShipTimestamp'] = format_date(time(), 'custom', 'c');
  $request['RequestedShipment']['ShipTimestamp'] = date('c');

  //
  // Packaging type - need this to be settable for each package rather
  // than one site-wide setting?
  //
  //$request['RequestedShipment']['PackagingType'] = variable_get('commerce_fedex_shipping_package_type', 'YOUR_PACKAGING');

  $request['RequestedShipment']['PackageDetail'] = 'INDIVIDUAL_PACKAGES';
  $request['RequestedShipment']['PackageCount']  = count($packages);
  $request['RequestedShipment']['RequestedPackageLineItems'] = $packages;

  //
  // Send the SOAP request to the FedEx server
  //
  try {
    $response = $client->getRates($request);

    if ($response->HighestSeverity != 'FAILURE' &&
        $response->HighestSeverity != 'ERROR')     {
      print_request_response($client);
    }
    else {
      drupal_set_message(t('Error in processing FedEx Shipping Quote transaction.'), 'error');
      foreach ($response->Notifications as $notification) {
        if (is_array($response->Notifications)) {
          drupal_set_message($notification->Severity . ': ' .
                             $notification->Message, 'error');
        }
        else {
          drupal_set_message($notification, 'error');
        }
      }
    }
    return $response;
  }
  catch (SoapFault $exception) {
    drupal_set_message('<h2>Fault</h2><br /><b>Code:</b>' . $exception->faultcode . '<br /><b>String:</b>' . $exception->faultstring . '<br />', 'error');
    // what else needs to be done here if FedEx quote fails?  What to display
    // to customer?
  }
}



/**
 * Convenience function to get FedEx codes for their package types.
 *
 * @return
 *   An array of human-friendly names for the different FedEx package types.
 */
function _commerce_fedex_shipping_package_types() {
  return array(
    'YOUR_PACKAGING' => t('Your Packaging'),
    'FEDEX_ENVELOPE' => t('FedEx Envelope'),
    'FEDEX_PAK'      => t('FedEx Pak'),
    'FEDEX_BOX'      => t('FedEx Box'),
    'FEDEX_TUBE'     => t('FedEx Tube'),
    'FEDEX_10KG_BOX' => t('FedEx 10kg Box'),
    'FEDEX_25KG_BOX' => t('FedEx 25kg Box'),
  );
}



/**
 * Returns an array of Countries serviced by FedEx.
 *
 * @return array
 *   The keyed values of countries that FedEx services.
 */
function commerce_fedex_shipping_serviced_countries() {
  return array(
    'AF' => 'Afghanistan',
    'AL' => 'Albania',
    'DZ' => 'Algeria',
    'AS' => 'American Samoa',
    'AD' => 'Andorra',
    'AO' => 'Angola',
    'AI' => 'Anguilla',
    'AG' => 'Antigua/Barbuda',
    'AR' => 'Argentina',
    'AM' => 'Armenia',
    'AW' => 'Aruba',
    'AU' => 'Australia',
    'AT' => 'Austria',
    'AZ' => 'Azerbaijan',
    'BS' => 'Bahamas',
    'BH' => 'Bahrain',
    'BD' => 'Bangladesh',
    'BB' => 'Barbados',
    'BY' => 'Belarus',
    'BE' => 'Belgium',
    'BZ' => 'Belize',
    'BJ' => 'Benin',
    'BM' => 'Bermuda',
    'BT' => 'Bhutan',
    'BO' => 'Bolivia',
    'BA' => 'Bosnia-Herzegovina',
    'BW' => 'Botswana',
    'BR' => 'Brazil',
    'VG' => 'British Virgin Islands',
    'BN' => 'Brunei',
    'BG' => 'Bulgaria',
    'BF' => 'Burkina Faso',
    'BI' => 'Burundi',
    'KH' => 'Cambodia',
    'CM' => 'Cameroon',
    'CA' => 'Canada',
    'CV' => 'Cape Verde',
    'KY' => 'Cayman Islands',
    'TD' => 'Chad',
    'CL' => 'Chile',
    'CN' => 'China',
    'CO' => 'Colombia',
    'CG' => 'Congo Brazzaville',
    'CD' => 'Congo Democratic Rep. of',
    'CK' => 'Cook Islands',
    'CR' => 'Costa Rica',
    'HR' => 'Croatia',
    'CY' => 'Cyprus',
    'CZ' => 'Czech Republic',
    'DK' => 'Denmark',
    'DJ' => 'Djibouti',
    'DM' => 'Dominica',
    'DO' => 'Dominican Republic',
    'TL' => 'East Timor',
    'EC' => 'Ecuador',
    'EG' => 'Egypt',
    'SV' => 'El Salvador',
    'GQ' => 'Equatorial Guinea',
    'ER' => 'Eritrea',
    'EE' => 'Estonia',
    'ET' => 'Ethiopia',
    'FO' => 'Faeroe Islands',
    'FJ' => 'Fiji',
    'FI' => 'Finland',
    'FR' => 'France',
    'GF' => 'French Guiana',
    'PF' => 'French Polynesia',
    'GA' => 'Gabon',
    'GM' => 'Gambia',
    'GE' => 'Georgia',
    'DE' => 'Germany',
    'GH' => 'Ghana',
    'GI' => 'Gibraltar',
    'GR' => 'Greece',
    'GL' => 'Greenland',
    'GD' => 'Grenada',
    'GP' => 'Guadeloupe',
    'GU' => 'Guam',
    'GT' => 'Guatemala',
    'GN' => 'Guinea',
    'GY' => 'Guyana',
    'HT' => 'Haiti',
    'HN' => 'Honduras',
    'HK' => 'Hong Kong',
    'HU' => 'Hungary',
    'IS' => 'Iceland',
    'IN' => 'India',
    'ID' => 'Indonesia',
    'IQ' => 'Iraq',
    'IE' => 'Ireland',
    'IL' => 'Israel',
    'IT' => 'Italy',
    'CI' => 'Ivory Coast',
    'JM' => 'Jamaica',
    'JP' => 'Japan',
    'JO' => 'Jordan',
    'KZ' => 'Kazakhstan',
    'KE' => 'Kenya',
    'KW' => 'Kuwait',
    'KG' => 'Kyrgyzstan',
    'LA' => 'Laos',
    'LV' => 'Latvia',
    'LB' => 'Lebanon',
    'LS' => 'Lesotho',
    'LR' => 'Liberia',
    'LY' => 'Libya',
    'LI' => 'Liechtenstein',
    'LT' => 'Lithuania',
    'LU' => 'Luxembourg',
    'MO' => 'Macau',
    'MK' => 'Macedonia',
    'MG' => 'Madagascar',
    'MW' => 'Malawi',
    'MY' => 'Malaysia',
    'MV' => 'Maldives',
    'ML' => 'Mali',
    'MT' => 'Malta',
    'MH' => 'Marshall Islands',
    'MQ' => 'Martinique',
    'MR' => 'Mauritania',
    'MU' => 'Mauritius',
    'MX' => 'Mexico',
    'FM' => 'Micronesia',
    'MD' => 'Moldova',
    'MC' => 'Monaco',
    'MN' => 'Mongolia',
    'ME' => 'Montenegro',
    'MS' => 'Montserrat',
    'MA' => 'Morocco',
    'MZ' => 'Mozambique',
    'NA' => 'Namibia',
    'NP' => 'Nepal',
    'NL' => 'Netherlands',
    'AN' => 'Netherlands Antilles',
    'NC' => 'New Caledonia',
    'NZ' => 'New Zealand',
    'NI' => 'Nicaragua',
    'NE' => 'Niger',
    'NG' => 'Nigeria',
    'NO' => 'Norway',
    'OM' => 'Oman',
    'PK' => 'Pakistan',
    'PW' => 'Palau',
    'PS' => 'Palestine Autonomous',
    'PA' => 'Panama',
    'PG' => 'Papua New Guinea',
    'PY' => 'Paraguay',
    'PE' => 'Peru',
    'PH' => 'Philippines',
    'PL' => 'Poland',
    'PT' => 'Portugal',
    'PR' => 'Puerto Rico',
    'QA' => 'Qatar',
    'RE' => 'Reunion',
    'RO' => 'Romania',
    'RU' => 'Russian Federation',
    'RW' => 'Rwanda',
    'MP' => 'Saipan',
    'WS' => 'Samoa',
    'SA' => 'Saudi Arabia',
    'SN' => 'Senegal',
    'RS' => 'Serbia',
    'SC' => 'Seychelles',
    'SG' => 'Singapore',
    'SK' => 'Slovak Republic',
    'SI' => 'Slovenia',
    'ZA' => 'South Africa',
    'KR' => 'South Korea',
    'ES' => 'Spain',
    'LK' => 'Sri Lanka',
    'KN' => 'St. Kitts/Nevis',
    'LC' => 'St. Lucia',
    'VC' => 'St. Vincent',
    'SR' => 'Suriname',
    'SZ' => 'Swaziland',
    'SE' => 'Sweden',
    'CH' => 'Switzerland',
    'SY' => 'Syria',
    'TW' => 'Taiwan',
    'TZ' => 'Tanzania',
    'TH' => 'Thailand',
    'TG' => 'Togo',
    'TO' => 'Tonga',
    'TT' => 'Trinidad/Tobago',
    'TN' => 'Tunisia',
    'TR' => 'Turkey',
    'TM' => 'Turkmenistan',
    'TC' => 'Turks & Caicos Islands',
    'VI' => 'U.S. Virgin Islands',
    'UG' => 'Uganda',
    'UA' => 'Ukraine',
    'AE' => 'United Arab Emirates',
    'GB' => 'United Kingdom',
    'US' => 'United States',
    'UY' => 'Uruguay',
    'UZ' => 'Uzbekistan',
    'VU' => 'Vanuatu',
    'VE' => 'Venezuela',
    'VN' => 'Vietnam',
    'WF' => 'Wallis & Futuna',
    'YE' => 'Yemen',
    'ZM' => 'Zambia',
    'ZW' => 'Zimbabwe',
  );
}

/**
 * Returns an array representing the available shipping services from FedEx that
 * this module offers.commerce_shipping_fedex_shipping_service_types
 *
 * @return array
 *   The keyed values of available shipping services.
 */
function commerce_fedex_shipping_shipping_service_types() {
  return array(
    'FEDEX_GROUND' => t('FedEx Ground'),
    'FEDEX_2_DAY' => t('FedEx 2 Day'),
    'FEDEX_2_DAY_AM' => t('FedEx 2 Day AM'),
    'FEDEX_EXPRESS_SAVER' => t('FedEx Express Saver'),
    'FIRST_OVERNIGHT' => t('FedEx First Overnight'),
    'GROUND_HOME_DELIVERY' => t('FedEx Ground Home Delivery'),
    'INTERNATIONAL_ECONOMY' => t('FedEx International Economy'),
    'INTERNATIONAL_FIRST' => t('FedEx International First'),
    'INTERNATIONAL_PRIORITY' => t('FedEx International Priority'),
    'PRIORITY_OVERNIGHT' => t('FedEx Priority Overnight'),
    'SMART_POST' => t('FedEx Smart Post'),
    'STANDARD_OVERNIGHT' => t('FedEx Standard Overnight'),
  );
}



/**
 * Returns an array of available FedEx package types.
 *
 * @return array
 *   The keyed values of available shipping package types.
 */
function commerce_fedex_package_types() {
  return array(
    'FEDEX_BOX' => t('FedEx Box'),
    'FEDEX_TUBE' => t('FedEx Tube'),
    'FEDEX_PAK' => t('FedEx Pak'),
    'FEDEX_ENVELOPE' => t('FedEx Envelope'),
    'YOUR_PACKAGING' => t('FedEx Customer Supplied Packaging'),
  );
}

/**
 * Returns an array of Drupal variables that are set by this module.
 *
 * @return array
 *   The variables that are set by this module.
 */
function commerce_fedex_settings_fields() {
  return array(
    'commerce_fedex_shipping_key',
    'commerce_fedex_shipping_password',
    'commerce_fedex_shipping_account_number',
    'commerce_fedex_shipping_meter_number',
    'commerce_fedex_shipping_request_mode',
    'commerce_fedex_shipping_company_name',
    'commerce_fedex_shipping_address_line_1',
    'commerce_fedex_shipping_address_line_2',
    'commerce_fedex_shipping_city',
    'commerce_fedex_shipping_state',
    'commerce_fedex_shipping_postal_code',
    'commerce_fedex_shipping_country_code',
    'commerce_fedex_shipping_services',
    'commerce_fedex_shipping_show_logo',
    'commerce_fedex_shipping_default_package_type',
    'commerce_fedex_shipping_default_package_size_length',
    'commerce_fedex_shipping_default_package_size_width',
    'commerce_fedex_shipping_default_package_size_height',
    'commerce_fedex_shipping_shipto_residential',
    'commerce_fedex_shipping_rate_service_type',
    'commerce_fedex_shipping_log',
    'commerce_fedex_shipping_dropoff',
    'commerce_fedex_shipping_insurance',
    'commerce_fedex_shipper_phone_number',
  );
}

/**
 * Returns an array of FedEx codes for dropoff and pickup.
 *
 * @return arary
 *   The keyed values of available dropoff types.
 */
function commerce_fedex_shipping_dropoff_types() {
  return array(
    'BUSINESS_SERVICE_CENTER' => t('Dropoff at FedEx Business Service Center'),
    'DROP_BOX' => t('Dropoff at FedEx Drop Box'),
    'REGULAR_PICKUP' => t('Regularly scheduled Pickup from your location'),
    'REQUEST_COURIER' => t('One-time Pickup request'),
    'STATION' => t('Dropoff at FedEx Staffed Location'),
  );
}


function commerce_fedex_create_rate_request($order) {
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

  $request['TransactionDetail'] = array('CustomerTransactionId' => ' *** Rate Request v13 using Drupal Commerce ***');
  $request['Version'] = array(
    'ServiceId' => 'crs',
    'Major' => '13',
    'Intermediate' => '0',
    'Minor' => '0',
  );

  // Load the number of packages and their physical attributes.
  $package_line_items = _commerce_fedex_get_package_items($order, $order_wrapper);

  // Make sure that there are packages to be sent in the request.
  if (!empty($package_line_items)) {
    $request['ReturnTransitAndCommit'] = TRUE;
    $request['RequestedShipment']['DropoffType'] = variable_get('commerce_fedex_shipping_dropoff', 'REGULAR_PICKUP');
    $request['RequestedShipment']['ShipTimestamp'] = date('c');
    $request['RequestedShipment']['PackagingType'] = variable_get('commerce_fedex_shipping_default_package_type', 'YOUR_PACKAGING');
    $request['RequestedShipment']['Shipper'] = _commerce_fedex_get_shipper();
    $request['RequestedShipment']['Recipient'] = _commerce_fedex_get_recipient($order, $order_wrapper);
    $request['RequestedShipment']['RateRequestTypes'] = strtoupper(variable_get('commerce_fedex_shipping_rate_service_type', 'list'));
    $request['RequestedShipment']['PackageDetail'] = 'INDIVIDUAL_PACKAGES';
    $request['RequestedShipment']['PackageCount'] = count($package_line_items);
    $request['RequestedShipment']['RequestedPackageLineItems'] = $package_line_items;

    // Return the request.
    return $request;
  }
  else {
    // If there are no shippable packages in the order.
    return FALSE;
  }
}


/**
 * Internal function to build shipper (ship from) array.
 *
 * @return array
 *   An array that represents the ship from address.
 */
function _commerce_fedex_shipping_get_shipper() {
  $shipper = array(
    'Contact' => array(
      'PersonName' => variable_get('commerce_fedex_shipping_company_name'),
      'CompanyName' => variable_get('commerce_fedex_shipping_company_name'),
    ),
    'Address' => array(
      'StreetLines' => array(variable_get('commerce_fedex_shipping_address_line_1')),
      'City' => variable_get('commerce_fedex_shipping_city'),
      'StateOrProvinceCode' => variable_get('commerce_fedex_shipping_state'),
      'PostalCode' => variable_get('commerce_fedex_shipping_postal_code'),
      'CountryCode' => variable_get('commerce_fedex_shipping_country_code'),
    ),
  );
  return $shipper;
}




/**
 * Prints SOAP request and response, iff allowed by user access permissions.
 *
 * To view transaction details, set display debug TRUE in
 * admin/store/settings/quotes/edit
 *
 * @param $client
 *   SOAP client object containing transaction history.
 */
function print_request_response($client) {
  if (user_access('configure quotes') &&
      variable_get('uc_quote_display_debug', FALSE)) {
    drupal_set_message('<h2>FedEx Transaction processed successfully.</h2>' .
                       '<h3>Request: </h3><pre>' .
                       check_plain($client->__getLastRequest())  . '</pre>' .
                       '<h3>Response: </h3><pre>' .
                       check_plain($client->__getLastResponse()) . '</pre>');
  }
}





function commerce_fedex_shipping_shipment_request($packages, $fedex_service, $destination) {



// Set up SOAP call.
  // Allow tracing so details of request can be retrieved for error logging
  $client = new SoapClient(drupal_get_path('module', 'commerce_fedex_shipping')
              . '/wsdl-' . variable_get('commerce_fedex_shipping_request_mode', 'testing')
              . '/ShipService_v7.wsdl', array('trace' => 1));

  // FedEx user key and password filled in by user on admin form
  $request['WebAuthenticationDetail'] = array(
    'UserCredential' => array(
      'Key'      => variable_get('commerce_fedex_shipping_key', 0),
      'Password' => variable_get('commerce_fedex_shipping_password', 0),
    )
  );

  // FedEx account and meter number filled in by user on admin form
  $request['ClientDetail'] = array(
      'AccountNumber' => variable_get('commerce_fedex_shipping_account_number', 0),
      'MeterNumber'   => variable_get('commerce_fedex_shipping_meter_number', 5),
  );
  

  // Optional parameter, contains anything
  $request['TransactionDetail'] = array(
    'CustomerTransactionId' => '*** Shipping Request v7 from Ubercart ***'
  );

  // Shipping Request v7.0.0
  $request['Version'] = array(
    'ServiceId'    => 'ship',
    'Major'        => '7',
    'Intermediate' => '0',
    'Minor'        => '0',
  );

  // Timestamp
  $request['RequestedShipment']['ShipTimestamp'] = date('c');

  // Drupal 6.x version of format_date() doesn't support 'c', so until
  // then we use date() directly.  date() doesn't take care of site
  // timezone, though.
  //$request['RequestedShipment']['ShipTimestamp'] = format_date(time(), 'custom', 'c');

  // Set Pickup/Dropoff type
  $request['RequestedShipment']['DropoffType'] = 'REGULAR_PICKUP';

  // Set Packaging type
  // First element of the packages array is used because all packages
  // in a multi-package shipment must have the same packaging type
  $request['RequestedShipment']['PackagingType'] = 'YOUR_PACKAGING';

  // Set Service
  $request['RequestedShipment']['ServiceType']   = $fedex_service;

  

  // Get address
  $request['RequestedShipment']['Shipper'] = _commerce_fedex_get_shipper();
  
  $request['RequestedShipment']['Shipper']['Contact']['PhoneNumber']=variable_get('commerce_fedex_shipper_phone_number', 0);


  // Grab details of package destination 
  $request['RequestedShipment']['Recipient'] = array(
    'Contact' => array(
      'PersonName'  => $destination->commerce_customer_address['und'][0]['name_line'],
      'CompanyName' => 'TTS',
      'PhoneNumber' => variable_get('commerce_fedex_shipper_phone_number', 0),
    ),
    'Address' => array(
      'StreetLines'         => $destination->commerce_customer_address['und'][0]['thoroughfare'],
      'City'                => $destination->commerce_customer_address['und'][0]['locality'],
      'StateOrProvinceCode' => $destination->commerce_customer_address['und'][0]['administrative_area'],
      'PostalCode'          => $destination->commerce_customer_address['und'][0]['postal_code'],
      'CountryCode'         => $destination->commerce_customer_address['und'][0]['country'],
      'Residential'         => 1,
    )
  );
  // CompanyName needs to be unset if empty
  if (empty($destination->company)) {
    unset($request['RequestedShipment']['Recipient']['Contact']['CompanyName']);
  }
 // Label specifications
  $request['RequestedShipment']['LabelSpecification'] = array(
    'LabelFormatType'          => 'COMMON2D',
    'ImageType'                => 'PDF',
    'LabelStockType'           => 'PAPER_7X4.75',
    'LabelPrintingOrientation' => 'TOP_EDGE_OF_TEXT_FIRST',
  );


  // Bill shipping to?
  $request['RequestedShipment']['ShippingChargesPayment'] = array(
    'PaymentType' => 'SENDER',
    'Payor'       => array(
      'AccountNumber' => variable_get('commerce_fedex_shipping_account_number', 0),
      'CountryCode'   => variable_get('commerce_fedex_shipping_country_code' , 'US'),
    ),
  );

  // Note that ACCOUNT rates *require* a valid account number
  // and return accurate answers on the production server
  $request['RequestedShipment']['RateRequestTypes'] = strtoupper(variable_get('commerce_fedex_shipping_rate_service_type', 'list'));

  $request['RequestedShipment']['PackageDetail'] = 'INDIVIDUAL_PACKAGES';
  $request['RequestedShipment']['PackageCount']  = count($packages);

  // Fill in Package details
  $request['RequestedShipment']['RequestedPackageLineItems'] = array();

  // Determine weight and length units to send to FedEx
  $weight_units = strtoupper('LB');
  $weight_conversion_factor = 1;
  

  // Iterate over $packages to account for multi-package shipments
  $sequence = 0;
  $package_properties = array();
  foreach ($packages as $package) {
      
   $sequence++;
   if($sequence >=1){
    $package_properties[$sequence] = array(
      'SequenceNumber'     => $sequence,
      'CustomerReferences' => array(
        '0' => array(
          'CustomerReferenceType' => 'INVOICE_NUMBER',
          // Gag - shouldn't be getting $order_id out of _SESSION
          'Value'                 => 1110,
        ),
        '1' => array(
          'CustomerReferenceType' => 'CUSTOMER_REFERENCE',
          'Value'                 => 01,
        ),
      ),
      // Weights must be rounded up to nearest integer value
      'Weight' => array(
        'Value'  => $package['Weight']['Value'],
        'Units'  => $package['Weight']['Units'],
      ),
      // Lengths must be rounded up to nearest integer value
      'Dimensions' => array(
        'Length' => $package['Dimensions']['Length'],
        'Width'  => $package['Dimensions']['Width'],
        'Height' => $package['Dimensions']['Height'],
        'Units'  => $package['Dimensions']['Units'],
      ),
    );
    // Only need Package Dimensions if using our own packaging.
    if ($request['RequestedShipment']['PackagingType'] != 'YOUR_PACKAGING') {
      unset($package_properties[$sequence]['Dimensions']);
    }

    

    // Multi-package shipments need to reference master tracking #
    if ($sequence > 1) {
      $request['RequestedShipment']['MasterTrackingId']  = $master_tracking_id;
    }

    // Fill in SOAP request with $package_properties
    $request['RequestedShipment']['RequestedPackageLineItems'][0] = $package_properties[$sequence];

    //
    // For Multi-Package shipments, need to send one SOAP request
    // *per package* to the FedEx server
    //
    try {
      $response[$sequence] = $client->processShipment($request);

      if ($response[$sequence]->HighestSeverity != 'FAILURE' &&
          $response[$sequence]->HighestSeverity != 'ERROR')     {
        print_request_response($client);

        // Save master tracking # to use for remaining package in shipment
        if ($sequence == 1 && count($packages) > 1) {
          $master_tracking_id = $response[$sequence]->CompletedShipmentDetail->MasterTrackingId;
        }
      }
      else {
        drupal_set_message(t('Error in processing FedEx Shipping Request.'), 'error');
        foreach ($response[$sequence]->Notifications as $notification) {
          if (is_array($response[$sequence]->Notifications)) {
            drupal_set_message($notification->Severity . ': ' . $notification->Message, 'error');
          }
          else {
            drupal_set_message($notification, 'error');
          }
        }
      }
    }
    catch (SoapFault $exception) {
      drupal_set_message('<h2>Fault</h2><br /><b>Code:</b>' . $exception->faultcode . '<br /><b>String:</b>' . $exception->faultstring . '<br />', 'error');
    }    
  }
  }
  return $response;
}



/**
     * Helper function to see array content by line break and indentation.
     */
    if (!function_exists('printr')) {

        function printr($array, $return=false) {
            $str = "<pre>";
            $str .= print_r($array, true);
            $str .= "</pre>";

            if ($return) {
                return $str;
            } else {
                echo $str;
            }
        }

    }
    
    
    function commerce_fedex_shipping_commerce_checkout_complete($order) {
         
   foreach ($order->commerce_line_items['und'] as $line) {
    $line_item = commerce_line_item_load($line['line_item_id']);
    
    }    
    $service = $line_item->commerce_shipping_service['und'][0]['value'];
    module_load_include('inc', 'commerce_fedex_shipping', 'commerce_fedex_shipping_helper');   
    $items = _commerce_fedex_get_package_items($order);
    $destination =  commerce_customer_profile_load($order->commerce_customer_shipping['und'][0]['profile_id']);
    $mp_response =   commerce_fedex_shipping_shipment_request($items, strtoupper($service), $destination); 
    foreach ($mp_response as $response) {
    // Package tracking numbers
    $tracking     = $response->CompletedShipmentDetail->CompletedPackageDetails->TrackingIds->TrackingNumber;
    if (isset($response->CompletedShipmentDetail->RoutingDetail->DeliveryDate)) {
      $exp_delivery = strtotime($response->CompletedShipmentDetail->RoutingDetail->DeliveryDate);
    }
    else {
      $exp_delivery = 0;
      // Ground and Home Delivery don't have DeliveryDate,
      // they have TransitTime instead
    }   
   
    $nid = db_insert('commerce_fedex_shipping_tracking') // Table name no longer needs {}
->fields(array(
  'oid' => $order->order_number,
  'tracking' => $tracking,
  'delivery_at' => $exp_delivery,
  'pkg_type'  => $service,
))
->execute();
    
    
  }
  

  

        
        
}

function commerce_fedex_shipping_preprocess_commerce_email_order_items(&$variables) {
  // Add a class to the list wrapper.
  
}

function commerce_fedex_shipping_theme_registry_alter(&$theme_registry) {
 
  if (isset($theme_registry['commerce_email_order_items'])) {

$theme_registry['commerce_email_order_items']['function'] = 'commerce_fedex_shipping_oreder_items';
  }
}

function commerce_fedex_shipping_oreder_items($variables) {
  $ordeid = $variables['commerce_order_wrapper']->order_id->value();
  // Create an object of type SelectQuery
    $query = db_select('commerce_fedex_shipping_tracking', 'c');

    // Add extra detail to this query object: a condition, fields and a range
    $query
      ->condition('c.oid', $ordeid)
      ->fields('c', array('tracking', 'delivery_at'));
  $result = $query->execute();
  $header = array(
    array('data' => t('Fedex Tracking id'), 'style' => array('text-align: left;')),
    array('data' => t('Delivery Date'), 'style' => array('text-align: left;')),

  );
    foreach ($result as $record) {
      $rows[] = array(
          'data' => array(
            array('data' =>$record->tracking , 'style' => array('text-align: left;')),
            array('data' => date('d-m-y', $record->delivery_at)  , 'style' => array('text-align: left;')),
           
          )
      );
    }
  
  
  
  
  $table = commerce_email_prepare_table($variables['commerce_order_wrapper']);
  $order_items = theme('table', $table);
  $order_items .= theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('style' => array('width: 50%; border: 1px solid #ddd;'))));

  return $order_items;
  
    }




